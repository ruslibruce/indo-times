import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Router from './src/router/Router';
import { DataContextProvider } from './src/hooks/DataContextProvider'

function App() {
  return (
    <DataContextProvider>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
    </DataContextProvider>
  );
};

export default App;