import AsyncStorage from "@react-native-async-storage/async-storage";

class TokenService {
    async getData() {
        return JSON.parse(await AsyncStorage.getItem("NEWS"));
    }
    async setData(berita) {
        console.log('berita di storage', JSON.stringify(berita));
        await AsyncStorage.setItem("NEWS", JSON.stringify(berita));
    }
    async removeData() {
        await AsyncStorage.removeItem("NEWS");
    }
    async getScreen() {
        return await AsyncStorage.getItem("SCREEN");
    }
    async setScreen(screen) {
        console.log('screen di storage', screen);
        await AsyncStorage.setItem("SCREEN", screen);
    }
}
export default new TokenService();
