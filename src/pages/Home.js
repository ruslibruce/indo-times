import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Image,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import React, {useState, useEffect, useCallback} from 'react';
import {Apikey} from '../constants/Apikey';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {COLORS} from '../constants/Colors';
import localStorage from '@react-native-async-storage/async-storage';
import {useFocusEffect} from '@react-navigation/native';
import NetInfo from '@react-native-community/netinfo';
import {useData} from '../hooks/DataContextProvider';
import BtnDelete from '../assets/svg/bt-delete.svg';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
}
function Home({navigation: {navigate}}) {
  const dataSave = useData();
  const [news, setNews] = useState(dataSave?.data?.dataResult ?? []);
  const [refreshing, setRefreshing] = useState(false);
  const [orientation, setOrientation] = useState(dataSave.screen ?? 'PORTRAIT');
  const [isConnected, setIsConnected] = useState(true);
  console.log('orientation home', orientation);
  // console.log('dataSave', JSON.stringify(dataSave));

  useEffect(() => {
    Dimensions.addEventListener('change', ({window: {width, height}}) => {
      if (width < height) {
        setOrientation('PORTRAIT');
        dataSave.set_screen('PORTRAIT');
      } else {
        setOrientation('LANDSCAPE');
        dataSave.set_screen('LANDSCAPE');
      }
    });
  }, []);

  useEffect(() => {
    // Update the document title using the browser API
    const unsubscribe = NetInfo.addEventListener(state => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
      if (!state.isConnected) {
        setIsConnected(false);
        if (news.length > 0) {
          dataSave.set_data(news);
        }
      } else {
        setIsConnected(true);
        getNews();
      }
    });
    return () => {
      console.log('KELUAR');
      unsubscribe();
    };
  }, []);

  async function getNews() {
    try {
      let response = await axios({
        method: 'get',
        url: `https://api.nytimes.com/svc/topstories/v2/science.json?api-key=${Apikey.apiKey}`,
      });
      const data = response.data;
      // console.log('getNews', JSON.stringify(data));
      setNews(data.results);
      setRefreshing(false);
    } catch (error) {
      console.log(error.response.data);
    }
  }

  function handleRefresh() {
    setRefreshing(true);
    getNews();
  }

  function handleDelete(value) {
    console.log('masuk delete', value);
    const items = [...news];
    items.splice(value, 1);
    setNews(items);
  }

  return (
    <SafeAreaView style={styles.container}>
      {!isConnected && <MiniOfflineSign />}
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: wp('5%'),
        }}>
        <FlatList
          keyExtractor={(item, index) => `${index}`}
          data={news}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={handleRefresh}
              colors={['#529F45', '#FFFFFF']}
            />
          }
          onEndReachedThreshold={0.5}
          initialNumToRender={10}
          contentContainerStyle={{
            justifyContent: 'space-between',
            // backgroundColor: COLORS.blue,
            // paddingHorizontal: wp('5%'),
          }}
          showsVerticalScrollIndicator={false}
          // showsHorizontalScrollIndicator={false}
          // horizontal={true}
          key={orientation}
          numColumns={orientation == 'LANDSCAPE' ? 2 : 1}
          pagingEnabled={true}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                key={index}
                style={
                  orientation == 'LANDSCAPE'
                    ? {
                        flexBasis: '48%',
                        marginRight: 10,
                      }
                    : {width: '100%'}
                }
                onPress={async () => {
                  navigate('DetailNews', {
                    news: item,
                    orientation: orientation,
                  });
                }}>
                <View style={styles.containerJudul}>
                  <Text style={styles.judul}>{item.title}</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: '100%',
                    borderColor: '#BCBCBC',
                    marginVertical: wp('2%'),
                  }}
                />
                <View style={styles.containerIsi}>
                  <Text style={styles.isiNews}>{item.abstract}</Text>
                </View>
                {item.multimedia ? (
                  <View style={{width: '100%', height: hp('30%')}}>
                    <Image
                      resizeMode="cover"
                      style={styles.imageNews}
                      source={{uri: item.multimedia[0].url}}
                    />
                    <BtnDelete
                      onPress={() => handleDelete(index)}
                      style={{position: 'absolute', right: 5, top: 5}}
                      width={wp('8%')}
                      height={hp('8%')}
                    />
                  </View>
                ) : (
                  <View
                    style={[
                      styles.imageNews,
                      {
                        backgroundColor: 'grey',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: hp('30%'),
                      },
                    ]}>
                    <Text style={{fontSize: hp('5%'), color: '#000000'}}>
                      {'No Data Image'}
                    </Text>
                    <BtnDelete
                      onPress={() => handleDelete(index)}
                      style={{position: 'absolute', right: 5, top: 2}}
                      width={wp('8%')}
                      height={hp('8%')}
                    />
                  </View>
                )}
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </SafeAreaView>
  );
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height: hp('100%'),
    // width: wp('100%'),
  },
  containerJudul: {
    height: hp('10%'),
    width: '100%',
  },
  containerIsi: {
    height: hp('10%'),
    width: '100%',
  },
  judul: {
    fontSize: hp('2.5%'),
    fontFamily: 'Roboto-Bold',
    textAlign: 'justify',
  },
  isiNews: {
    fontSize: hp('2%'),
    fontFamily: 'Roboto-Regular',
  },
  imageNews: {
    width: '100%',
    height: '100%',
    borderRadius: hp('3%'),
  },
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width,
    position: 'absolute',
    top: 30,
  },
  offlineText: {color: '#fff'},
});
