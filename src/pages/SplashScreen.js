import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import React, {useEffect} from 'react';
import Splash from '../assets/images/splashscreen.png';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

function SplashScreen({navigation: {navigate}}) {
  useEffect(() => {
    // Update the document title using the browser API
    setTimeout(() => {
      navigate('Home');
    }, 5000);
  });

  return (
    <ImageBackground
      style={styles.background}
      source={Splash}
      resizeMode="contain"></ImageBackground>
  );
}

export default SplashScreen;

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
});
