import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Image,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import React, {useState, useEffect, useCallback, useRef} from 'react';
import Apikey from '../constants/Apikey';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import localStorage from '@react-native-async-storage/async-storage';
import RenderHtml from 'react-native-render-html';
import {WebView} from 'react-native-webview';
import {useFocusEffect} from '@react-navigation/native';
import {useData} from '../hooks/DataContextProvider';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import NetInfo from '@react-native-community/netinfo';
function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
}
function DetailNews({navigation: {navigate}, route}) {
  const dataSave = useData();
  const {news} = route.params;
  const [refreshing, setRefreshing] = useState(false);
  const [orientation, setOrientation] = useState(dataSave.screen ?? 'PORTRAIT');
  const [isConnected, setIsConnected] = useState(true);
  const webRef = useRef();
  console.log('orientation detail', orientation);

  useEffect(() => {
    Dimensions.addEventListener('change', ({window: {width, height}}) => {
      if (width < height) {
        setOrientation('PORTRAIT');
        dataSave.set_screen('PORTRAIT');
      } else {
        setOrientation('LANDSCAPE');
        dataSave.set_screen('LANDSCAPE');
      }
    });
  }, []);

  useEffect(() => {
    // Update the document title using the browser API
    const unsubscribe = NetInfo.addEventListener(state => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
      if (state.isConnected) {
        webRef.current.reload();
        setIsConnected(true);
      } else {
        setIsConnected(false);
      }
    });
    return () => {
      console.log('KELUAR');
      unsubscribe();
    };
  }, []);

  return (
    <SafeAreaView style={{flex: 1}}>
      {!isConnected && <MiniOfflineSign />}
      <WebView
        ref={WEBVIEW_REF => (webRef.current = WEBVIEW_REF)}
        style={
          orientation === 'LANDSCAPE'
            ? {position: 'absolute', left: 0, right: 0, top: 0, bottom: 0}
            : {width: '100%'}
        }
        source={{uri: `${news.url}`}}
      />
    </SafeAreaView>
  );
}

export default DetailNews;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height: hp('100%'),
    // width: wp('100%'),
  },
  containerJudul: {
    height: hp('10%'),
    width: wp('100%'),
  },
  containerIsi: {
    height: hp('10%'),
    width: wp('100%'),
  },
  judul: {
    fontSize: hp('2.5%'),
    fontFamily: 'Roboto-Bold',
    textAlign: 'justify',
  },
  isiNews: {
    height: hp('100%'),
  },
  imageNews: {
    width: wp('100%'),
    height: hp('75%'),
  },
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width,
    position: 'absolute',
    top: 30,
  },
  offlineText: {color: '#fff'},
});
