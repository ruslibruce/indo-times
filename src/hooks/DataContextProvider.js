import { View, Text } from 'react-native'
import React, { useEffect, useMemo, useReducer } from 'react'
import tokenServices from '../services/token.services';
import { createAction } from '../utility/CreateAction'
import { sleep } from '../utility/Sleep'
import { DataContext } from '../context/DataContext'
const initialState = {
    data: null,
    screen: null
};

const dataReducer = (state, action) => {
    switch (action.type) {
        case 'SET_DATA':
            return {
                ...state,
                data: action.payload
            }
        case 'REMOVE_DATA':
            return {
                ...state,
                data: action.payload
            }
        case 'SET_SCREEN':
            return {
                ...state,
                screen: action.payload
            }
        default:
            return { ...state, ...action.payload };
    }
}

const DataContextProvider = ({ children }) => {
    const [offlineState, dispatch] = useReducer(dataReducer, initialState)
    const offlineUse = useMemo(() => ({
        ...offlineState,
        set_data: async dataResult => {
            // console.log('dataResult', dataResult)
            let temp = {
                dataResult: dataResult
            }
            // console.log('data', temp)
            await tokenServices.setData(temp);
            dispatch(createAction('SET_DATA', temp))
        },
        set_screen: async dataResult => {
            await tokenServices.setScreen(dataResult);
            console.log('screen context', dataResult)
            const hasilData = dataResult
            dispatch(createAction('SET_SCREEN', hasilData))
        }
    }))
    useEffect(() => {
        sleep(2000).then(async () => {
            const dataLocal = await tokenServices.getData()
            if (dataLocal != null) {
                console.log('dataLocal', dataLocal)
                const data = { ...dataLocal }
                dispatch(createAction('SET_DATA', data));
            } else {
                return
            }
            const dataScreen = await tokenServices.getScreen()
            if (dataScreen) {
                dispatch(createAction('SET_SCREEN', dataScreen));
            } else {
                return
            }

        })
    }, [])

    return (
        <DataContext.Provider value={{ ...offlineUse }}>
            {children}
        </DataContext.Provider>
    )
}

const useData = () => React.useContext(DataContext);

export { DataContextProvider, useData }